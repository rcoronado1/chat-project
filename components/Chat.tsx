import { useEffect, useState, useRef } from 'react'
// import { flushSync } from 'react-dom'
import { ChatLine, ROLE_ASSISTANT, ROLE_USER, ChatGPTMessage, LoadingChatLine, StartChatLine } from '../components/ChatLine'
import { ChatInput } from '../components/ChatInput'

import { 
  selectComments, addComment, selectInitComments, 
  resetComments, selectStartChat, 
  initChat, selectPayloadChatGPT,
  selectPayloadPrompt, selectActivePrompt
} from "../store/slices/commentSlice"
import { useDispatch, useSelector } from "react-redux"
import { useCookies } from 'react-cookie'

const COOKIE_NAME = 'nextjs-example-ai-chat-gpt3'


export default function Chat() {
  // const listRef = useRef<HTMLUListElement | null>(null)
  const [loading, setLoading] = useState(false)
  const [errorMsg, setErrorMsg] = useState('')

  // const [startChat, setStartChat] = useState(false)

  const [cookie, setCookie] = useCookies([COOKIE_NAME])
  const listRef = useRef(null);
  const commentsState = useSelector(selectComments)
  const initCommentsState = useSelector(selectInitComments)
  const startChat = useSelector(selectStartChat)
  const payloadChatGPT = useSelector(selectPayloadChatGPT)
  const payloadPrompt = useSelector(selectPayloadPrompt)
  const activePrompt = useSelector(selectActivePrompt)


  const dispatch = useDispatch()
  const local_messages = commentsState


  useEffect(() => {
    // console.log('useEffect')
    if (!cookie[COOKIE_NAME]) {
      const randomId = Math.random().toString(36).substring(7)
      setCookie(COOKIE_NAME, randomId)
    }
  }, [cookie, setCookie])

  const callApi = async (messages: any) => {
    // api
    console.log('callApi')
    console.log(messages)
    const response = await fetch('/api/chat', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        messages: messages,
        user: cookie[COOKIE_NAME],
        ...payloadChatGPT
      }),
    })
    console.log('Edge function returned.')
    if (!response.ok) {
      throw new Error(response.statusText)
    }

    // This data is a ReadableStream
    const data = response.body
    if (!data) {
      return
    }

    const reader = data.getReader()
    const decoder = new TextDecoder()

    const { value, done: doneReading } = await reader.read()
    const chunkValue = decoder.decode(value)
    // console.log('chunkValue:',chunkValue)
    let jsonObject = JSON.parse(chunkValue)
    let botMessage = jsonObject.choices[0].message
    // console.log(botMessage)
    return botMessage
  }


  const sendMessage = async (ref: any, role: any, message: string) => {
    // console.log('initCommentsState:',initCommentsState)
    // console.log('sendMessage:',payloadPrompt)

    let lastMessages
    if (activePrompt){
      lastMessages = [...payloadPrompt, ...local_messages]//.slice(-10)
    } else {
      lastMessages = [...initCommentsState, ...local_messages]//.slice(-10)
    }

    // let lastMessages = [...initCommentsState, ...local_messages]//.slice(-10)
    lastMessages.push({'role': role, 'content': message})
    // console.log(cookie[COOKIE_NAME])

    // Loading
    const myPromiseLoading = ()=>Promise.resolve(setLoading(true))
    myPromiseLoading().then(()=>{
      // Scroll Bottom
      ref.current.scrollTo({
        top: ref.current.scrollHeight,
        behavior: 'smooth',
      })
    })

    // api
    let botMessage = await callApi(lastMessages)
    console.log(botMessage)
    setLoading(false)
    if (!botMessage) {
      return
    }
    
    const myPromise = ()=>Promise.resolve(dispatch(addComment(botMessage)))
    myPromise().then(()=>{
      // Scroll Bottom
      ref.current.scrollTo({
        top: ref.current.scrollHeight,
        behavior: 'smooth',
      })
    })
  }

  const cleanChat = async () => {
    dispatch(resetComments())
    setLoading(false)
    dispatch(initChat(false))
  }

  const arrayVacio = (arr: any) => !Array.isArray(arr) || arr.length === 0;

  const eventStartChat = async () => {

    if (activePrompt && arrayVacio(payloadPrompt)){
      setErrorMsg('El prompt esta vacio, requiere [[system]]')
      return
    }    
    setErrorMsg('')


    let lastMessages
    if (activePrompt){
      lastMessages = [...payloadPrompt]//.slice(-10)
    } else {
      lastMessages = [...initCommentsState]//.slice(-10)
    }
    setLoading(true)
    let botMessage = await callApi(lastMessages)
    setLoading(false)
    dispatch(addComment(botMessage))
    dispatch(initChat(true))
  }

  let aux = `
> #### The quarterly results look great!
>
> - Revenue was off the chart.
> - Profits were higher than ever.
>
>  *Everything* is going according to **plan**.
  `

  return (
    <>
      <div ref={listRef} className="flex flex-col h-full overflow-x-auto mb-4 md:px-4 lg:px-12 pt-4">
        <div className="flex flex-col h-full">
          <div className="grid grid-cols-12 gap-y-2">
              {(!startChat && !loading) && <StartChatLine startChat={eventStartChat} msgError={errorMsg} />}

              {local_messages ? local_messages.map(({ content, role }: ChatGPTMessage, index:any) => (
                  <ChatLine key={index} role={role} content={content} />
              )): null}
        
              {loading && <LoadingChatLine />}

          </div>
        </div>
      </div>
      <ChatInput 
        isLoading={loading || !startChat} 
        sendMessage={(role: any, message:any)=>sendMessage(listRef, role, message)}
        cleanChat={cleanChat}
      />

    </>
  )
}