import React from "react";
import { useState } from 'react'
import { ChevronDownIcon } from '@heroicons/react/20/solid'
import { Switch } from '@headlessui/react'

import { 
  selectPayloadChatGPT, selectPayloadPrompt, 
  updatePayload, updatePayloadPrompt, resetPayload,
  selectComments, selectActivePrompt, updateActivePrompt
} from "../../store/slices/commentSlice"
import { useDispatch, useSelector } from "react-redux"

const NavForm = () => {
  const payloadChatGPT = useSelector(selectPayloadChatGPT)
  const payloadPrompt = useSelector(selectPayloadPrompt)
  const comments = useSelector(selectComments)
  const _activePrompt_ = useSelector(selectActivePrompt)


  const local_payloadChatGPT = {...payloadChatGPT}

  // const [temperature, setTemperature] = useState(payloadChatGPT.temperature)
  // const [maxTokens, setMaxTokens] = useState(payloadChatGPT.max_tokens)
  // const [topP, setTopP] = useState(payloadChatGPT.top_p)
  // const [frequencyPenalty, setFrequencyPenalty] = useState(payloadChatGPT.frequency_penalty)
  // const [presencePenalty, setPresencePenalty] = useState(payloadChatGPT.presence_penalty)
  const [activePrompt, setActivePrompt] = useState(_activePrompt_)
  const [newPrompt, setNewPrompt] = useState('')
  const [errorMsg, setErrorMsg] = useState('')

  

  const dispatch = useDispatch()

  const changeValues = (event:any, key:any) => {
    // setValues(event.target.value)
    const value = key == 'max_tokens' ? parseInt(event.target.value) : parseFloat(event.target.value)
    local_payloadChatGPT[key] = value
    dispatch(updatePayload(local_payloadChatGPT))
  };

  const splitPrompt = (value: string) => {
    // const texto = "[[system]]: Eres un asistente ...\n[[user]]: Hola como te llamas?\n[[assistant]]: Me llamo Chappi";
    // const regex = /\[\[(.*?)\]\]:/g;
    const texto = value + ' '
    const regex = /\[\[(system|user|assistant)\]\]/g;
    const matches = texto.matchAll(regex)
    // console.log(texto)
    const results = Array.from(matches, match => {
      const role = match[1]
      const startIndex = (match.index ?? 0) + match[0].length
      const endIndex = texto.indexOf("[[", startIndex)
      const content = texto.slice(startIndex, endIndex).trim()
      // console.log(role)
      // console.log(startIndex, endIndex)

      return { role, content }
    });
    return results
  }

  const changePrompt = () => {

    if (comments.length > 0) {
      setErrorMsg('Necesitas limpiar el Chat.')
      return
    }

    const new_messages = splitPrompt(newPrompt)
    // console.log('changePrompt:',new_messages)
    if (new_messages.length>0) {
      dispatch(updatePayloadPrompt(new_messages))
      setErrorMsg('')
    } else {
      setErrorMsg('El prompt no tiene el formato adecuado.')
    }
  }


  const handleSwitchPrompt = (e: any) => {
    if (comments.length > 0) {
      setErrorMsg('Necesitas limpiar el Chat.')
      return
    }
    dispatch(updateActivePrompt(e.target.checked))
    setActivePrompt(e.target.checked)
    setErrorMsg('')
    if (!e.target.checked){
      dispatch(updatePayloadPrompt([]))
    } else {
      changePrompt()
    }
  }

  return (
  <form action="#" method="POST" className="mx-auto max-w-xl dark">
      <div className="grid grid-cols-1 gap-y-6 gap-x-8 sm:grid-cols-2">
        <div className="sm:col-span-2">
          <label htmlFor="company" className="block text-sm font-semibold leading-6 text-gray-900 dark:text-gray-200">
            Model
          </label>
          <div className="mt-1">
            <input
              type="text"
              name="company"
              id="company"
              disabled
              value="gpt-3.5-turbo"
              className="block w-full rounded-md border-0 py-2 px-3 text-gray-900 focus:outline-none ring-1 ring-inset ring-gray-800 focus:ring-gray-700 sm:text-sm sm:leading-6 dark:text-gray-200 dark:bg-black"
            />
          </div>
        </div>
        <div className="sm:col-span-2">
          <label htmlFor="large-range" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
            Temperature <span className="float-right">[ {payloadChatGPT.temperature} ]</span>
          </label>
          <div className="mt-1">
            <input 
              id="temperature" 
              type="range"
              value={payloadChatGPT.temperature}
              onChange={(e)=>{changeValues(e, 'temperature')}}
              min="0" max="1" step="0.01" 
              className="w-full h-1 accent-gray-300 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm focus:outline-none dark:bg-gray-700 ring-1 ring-inset ring-gray-800 focus:ring-gray-700" />
          </div>
        </div>

        <div className="sm:col-span-2">
          <label htmlFor="large-range" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
            Max Tokens <span className="float-right">[ {payloadChatGPT.max_tokens} ]</span>
          </label>
          <div className="mt-1">
            <input 
              id="maxTokens" 
              type="range"
              value={payloadChatGPT.max_tokens}
              onChange={(e)=>{changeValues(e, 'max_tokens')}}
              min="1" max="1024" step="1" 
              className="w-full h-1 accent-gray-300 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm focus:outline-none dark:bg-gray-700 ring-1 ring-inset ring-gray-800 focus:ring-gray-700" />
          </div>
        </div>

        <div className="sm:col-span-2">
          <label htmlFor="large-range" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
            Top P  <span className="float-right">[ {payloadChatGPT.top_p} ]</span>
          </label>
          <div className="mt-1">
            <input 
              id="topP" 
              type="range"
              value={payloadChatGPT.top_p}
              onChange={(e)=>{changeValues(e, 'top_p')}}
              min="0" max="1" step="0.01" 
              className="w-full h-1 accent-gray-300 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm focus:outline-none dark:bg-gray-700 ring-1 ring-inset ring-gray-800 focus:ring-gray-700" />
          </div>
        </div>

        <div className="sm:col-span-2">
          <label htmlFor="large-range" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
            Frequency Penalty  <span className="float-right">[ {payloadChatGPT.frequency_penalty} ]</span>
          </label>
          <div className="mt-1">
            <input 
              id="frequencyPenalty" 
              type="range"
              value={payloadChatGPT.frequency_penalty}
              onChange={(e)=>{changeValues(e, 'frequency_penalty')}}
              min="0" max="2" step="0.01" 
              className="w-full h-1 accent-gray-300 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm focus:outline-none dark:bg-gray-700 ring-1 ring-inset ring-gray-800 focus:ring-gray-700" />
          </div>
        </div>

        <div className="sm:col-span-2">
          <label htmlFor="large-range" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">
            Presence Penalty  <span className="float-right">[ {payloadChatGPT.presence_penalty} ]</span>
          </label>
          <div className="mt-1">
            <input 
              id="presencePenalty" 
              type="range"
              value={payloadChatGPT.presence_penalty}
              onChange={(e)=>{changeValues(e, 'presence_penalty')}}
              min="0" max="2" step="0.01" 
              className="w-full h-1 accent-gray-300 bg-gray-200 rounded-lg appearance-none cursor-pointer range-sm focus:outline-none dark:bg-gray-700 ring-1 ring-inset ring-gray-800 focus:ring-gray-700" />
          </div>
        </div>
        <div className="sm:col-span-2">
          <button
            type="button"
            onClick={()=>dispatch(resetPayload())}
            className="block w-full rounded-md bg-black border-gray-800 border-spacing-1 border-2 px-3.5 py-2.5 text-center text-sm text-white shadow-sm hover:bg-gray-700  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
          >
            Reset
          </button>
        </div>

        <div className="sm:col-span-2">
          <label className="block text-sm font-semibold leading-6 text-gray-900 dark:text-white">
            Custom Prompt
            <div className="relative inline-flex items-center cursor-pointer float-right">
              <input type="checkbox" checked={activePrompt} onChange={(e)=>{handleSwitchPrompt(e)}} className="sr-only peer"/>
              <div className="w-9 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-gray-300 dark:peer-focus:ring-0 rounded-full peer dark:bg-red-600 peer-checked:after:translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-[2px] after:left-[2px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-green-600"></div>
            </div>
          </label>
          <span className="text-red-600 text-xs">{ errorMsg }</span>
          <div className="mt-1">
            <textarea
              name="prompt"
              id="prompt"
              rows={4}
              value={newPrompt}
              onChange={(e)=>{setNewPrompt(e.target.value)}}
              disabled={!activePrompt}
              className="resize-none block w-full rounded-md border-0 py-2 px-3.5 text-gray-900 focus:outline-none ring-1 ring-inset ring-gray-800 focus:ring-gray-700 sm:text-sm sm:leading-6 dark:text-gray-200 dark:bg-black disabled:opacity-30"
              placeholder={`[[system]]: Eres un asistente ...\n[[user]]: Hola como te llamas?\n[[assistant]]: Me llamo Chappi`}
            />
            { activePrompt ? 
              <button
                type="button"
                onClick={()=>{changePrompt()}}
                className="block w-full rounded-md bg-red-600 border-gray-800 border-spacing-1 border-2 px-3.5 py-2.5 text-center text-sm text-white shadow-sm hover:bg-gray-700  focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 "
              >
                Asigna prompt
              </button>: null
            }
          </div>
        </div>
      </div>
  </form>
  )
}

export default NavForm;
