import Image from 'next/image'
import styles from '@/styles/Home.module.css'
import md from 'markdown-it'
import { PaperAirplaneIcon } from "@heroicons/react/24/outline";
import { selectActivePrompt} from "../store/slices/commentSlice"
import { useDispatch, useSelector } from "react-redux"


export const ROLE_USER = 'user'
export const ROLE_ASSISTANT = 'assistant'
export const ROLE_SYSTEM = 'system'

export interface ChatGPTMessage {
    role: string
    content: string
  }

// loading placeholder animation for the chat line
export const LoadingChatLine = () => {
    let cls_div_role, cls_div_div_role, cls_msg, path_avatar
    cls_div_role = "col-start-1 col-end-8"
    cls_div_div_role = "flex-row items-center"
    cls_msg = "ml-3 bg-white"
    path_avatar = "/chatbot.png"
    return (
        <div className={cls_div_role + " p-3 rounded-lg"}>
            <div className={"flex " + cls_div_div_role}>
                <div className="flex items-center justify-center h-10 w-10 rounded-full flex-shrink-0">
                    <Image
                        src={path_avatar}
                        alt="Vercel Logo"
                        width={100}
                        height={24}
                        priority
                    />
                </div>
                <div className={"relative text-sm " + cls_msg +  " py-2 px-4 shadow rounded-xl"}>
                    <div>Escribiendo una respuesta <p className={styles.dot7 + ' dots-7'}></p></div>
                    
                </div>
            </div>
        </div>
    )
}

export const StartChatLine = ({startChat, msgError}:any) => {
    const activePrompt = useSelector(selectActivePrompt)

    let cls_div_role, cls_div_div_role, cls_msg, path_avatar
    cls_div_role = "col-start-1 col-end-8"
    cls_div_div_role = "flex-row items-center"
    cls_msg = "ml-3 bg-white"
    path_avatar = "/chatbot.png"
    return (
        <div className={cls_div_role + " p-3 rounded-lg"}>
            <div className={"flex " + cls_div_div_role}>
                <div className="flex items-center justify-center h-10 w-10 rounded-full flex-shrink-0">
                    <Image
                        src={path_avatar}
                        alt="Vercel Logo"
                        width={100}
                        height={24}
                        priority
                    />
                </div>
                <div className={"relative text-sm " + cls_msg +  " py-2 px-4 shadow rounded-xl"}>
                    { !activePrompt ? 
                        <div>Hola soy el Asistente Virtual de la Universidad UTP</div> :
                        <div>Hola soy tu Chat Bot</div>
                    }
                    <div className='pt-1'>
                    <button
                        className={"flex items-center justify-center  rounded-xl text-white px-4 py-1 flex-shrink-0 bg-gray-900"}
                        onClick={() => {startChat()}}
                        >
                        <span>Iniciar</span>
                        <span className="ml-2">
                            <PaperAirplaneIcon className="w-4 h-4 -mt-px"/>
                        </span>
                        </button>
                    </div>
                    <span className="text-red-600 text-xs">{ msgError }</span>                  
                </div>
            </div>
        </div>
    )
}


export function ChatLine({role, content}:ChatGPTMessage){

    // <div className="col-start-6 col-end-13 p-3 rounded-lg">
    // <div className="flex items-center justify-start flex-row-reverse">
    let cls_div_role, cls_div_div_role, cls_msg, path_avatar, cls_md
    if (role==ROLE_USER){
        // derecha
        cls_div_role = "col-start-3 md:col-start-3 lg:col-start-5 col-end-13"
        // cls_div_role = "col-start-3 col-end-13"
        cls_div_div_role = "items-center justify-start flex-row-reverse"
        cls_msg = "mr-3 bg-gray-900 text-white break-words"
        path_avatar = "/male.png"
        cls_md = 'prose-invert'
    } else {
        // izquierda
        cls_div_role = "col-start-1 col-end-12 md:col-end-10 lg:col-end-8"
        // cls_div_role = "col-start-1 col-end-10"
        cls_div_div_role = "flex-row items-center"
        cls_msg = "ml-3 bg-white"
        path_avatar = "/chatbot.png"
        cls_md = 'prose'
    }

    return (
        <div className={cls_div_role + " p-3 rounded-lg"}>
            <div className={"flex " + cls_div_div_role}>
                <div className="flex items-center justify-center h-10 w-10 rounded-full flex-shrink-0">
                    <Image
                        src={path_avatar}
                        alt="Vercel Logo"
                        // className={styles.vercelLogo}
                        width={100}
                        height={24}
                        priority
                    />
                </div>
                <div className={"relative text-sm " + cls_msg +  " py-2 px-4 shadow rounded-xl max-w-[84%] " + cls_md}>
                {
                    role==ROLE_USER ?
                        <div className="whitespace-pre-wrap">{content}</div> 
                        : 
                        <div dangerouslySetInnerHTML={{ __html: md().render(content) }} /> 
                }                       
                </div>
            </div>
        </div>
    )
}