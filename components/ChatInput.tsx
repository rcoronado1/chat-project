import { useEffect, useState, useRef } from 'react'
import { ChatLine, ROLE_ASSISTANT, ROLE_USER, ChatGPTMessage } from '../components/ChatLine'
import { selectComments, addComment } from "../store/slices/commentSlice"
import { useDispatch, useSelector } from "react-redux"
import { TrashIcon, PaperAirplaneIcon } from "@heroicons/react/24/outline"
import MobileDetect from 'mobile-detect'

import useAutosizeTextArea from "./useAutoSizeTextarea";

export function ChatInput({isLoading, sendMessage, cleanChat}:any) {
    const role = ROLE_USER
    const [message, setmessage] = useState("")
    const dispatch = useDispatch()

    const textAreaRef = useRef<HTMLTextAreaElement>(null)

    const md = new MobileDetect(window.navigator.userAgent);
    const isMobile = md.mobile();

  
    useAutosizeTextArea(textAreaRef.current, message)
    const handleChange = (evt: React.ChangeEvent<HTMLTextAreaElement>) => {
      const val = evt.target?.value;
      setmessage(val);
    };
    

    const handleAddMessage = (role:any, content:any) => {
      const local_content = content.trim()
      const myPromise = ()=>Promise.resolve(dispatch(addComment({'role': role, 'content': local_content})))
      myPromise().then(()=>{
        setmessage('')
        sendMessage(role, local_content)
      })
    }

    return (
        <div className="flex flex-row items-center h-auto rounded-xl shadow bg-white p-4 mx-4 sm:mx-4 lg:mx-28 mb-4 md:mb-4">
          <div>
            <button className="flex items-center justify-center text-gray-400 hover:text-gray-600" title='Limpiar'
              onClick={() => {cleanChat()}}
              disabled={isLoading}
            >
              <TrashIcon className="w-6 h-6"/>
            </button>
          </div>
          <div className="flex-grow ml-4">
            <div className="relative w-full border rounded-md pl-4 py-2">
              <textarea
                className="flex w-full focus:outline-none focus:border-gray-400 max-h-24 resize-none "
                value={message}
                placeholder={ 'Ingresa tu consulta' + (isMobile ? '' : ', [Shift + Enter] para salto de línea')}
                disabled={isLoading}
                onChange={handleChange}
                ref={textAreaRef}
                rows={1}
                onKeyDown={(e) => {
                  if (e.key === 'Enter' && message.trim().length > 3 && !isLoading) {
                    if(e.shiftKey)
                    {
                      console.log('shift enter was pressed');
                    } else {
                      e.preventDefault()
                      handleAddMessage(role, message)
                    }
                  }
                }}
                id="message" name="message" required 
              />
            </div>
          </div>
          <div className="ml-2">
            <button
              className={
                "flex items-center justify-center  rounded-lg text-white flex-shrink-0 " + 
                (message.trim().length <= 3 ? 'bg-gray-600' : 'bg-gray-900 hover:bg-gray-800')
              }
              disabled={message.trim().length <= 3 || isLoading}
              onClick={() => {handleAddMessage(role, message)}}
            >
              {/* <span>Enviar</span> */}
              <span className="p-3 rounded-lg">
                <PaperAirplaneIcon className="w-4 h-4 -mt-px"/>
              </span>
            </button>
          </div>
        </div>
    )
  }