import { type ChatGPTMessage } from '../../components/ChatLine'
import { OpenAI, OpenAIPayload } from '../../utils/OpenAI'
import { selectPayloadChatGPT, selectPayloadPrompt } from "../../store/slices/commentSlice"
import { useSelector } from "react-redux"

// break the app if the API key is missing
if (!process.env.OPENAI_API_KEY) {
  throw new Error('Missing Environment Variable OPENAI_API_KEY')
}

export const config = {
  runtime: 'edge',
}

const handler = async (req: Request): Promise<Response> => {
  // const payloadChatGPT = useSelector(selectPayloadChatGPT)

  const body = await req.json()
  const messages: ChatGPTMessage[] = [...body?.messages ]

  const payload: OpenAIPayload = {
    model: 'gpt-3.5-turbo',
    messages: messages,
    temperature: body?.temperature,
    max_tokens: body?.max_tokens,
    top_p: body?.top_p,
    frequency_penalty: body?.frequency_penalty,
    presence_penalty: body?.presence_penalty,
    user: body?.user,
    stream: false,
    n: 1,
  }

  const response = await OpenAI(payload)
  return response
}
export default handler
