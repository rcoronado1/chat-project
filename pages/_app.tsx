import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { wrapper } from "../store/store"
import Layout from "../components/layout/Layout";
import { PersistGate } from "redux-persist/integration/react";
import { useStore } from "react-redux";

function MyApp({ Component, pageProps }: AppProps) {
  // return <Component {...pageProps} />
  const store: any = useStore();
  return (
    <PersistGate persistor={store.__persistor} loading={<div>Loading</div>}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </PersistGate>
  );
}

export default wrapper.withRedux(MyApp)
