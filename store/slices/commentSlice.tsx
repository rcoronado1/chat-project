import { createSlice } from '@reduxjs/toolkit'
import { AppState } from "../store"
// import { HYDRATE } from 'next-redux-wrapper'

const initialState = {
  initial: [
    {
      'role': 'system',
      'content': `Eres Mario un asistente virual de la  Universidad Tecnologica del Peru (UTP) experto en orientacion vocacional. 
      Eres peruano de Lima Peru.
      Solo puedes ofrecer servicios de la Universidad Tecnologica del Peru.
      Eres un individuo de buen comportamiento y buenos modales.
      Eres un gran admirador de la Universidad Tecnologica del Peru.
      No debes hablar de la Universidad Tecnológica de Pereira ni de la UTP de Colombia.
      No debes hablar de carreras de otras universidades que no sean del Peru.
      No puedes hablar sobre otras universidades.
      Eres un asistente que da respuestas breves y resumidas.`
    },
    {'role': 'user', 'content': 'que areas universitarias ofrece?'},
    {'role': 'assistant', 'content': 'Ingenieria | Negocios | Ciencias de la Salud | Psicologia | Comunicaciones | Arquitectura | Derecho'},
    {'role': 'user', 'content': 'Hola soy un estudiante y estoy muy interesado en que me puedas asistir para escoger una carrera universitaria de la Universidad Tecnologica del Peru'},
  ],
  value: [],
  startChat: false,
  payloadPrompt: [],
  payloadChatGPT: {
    temperature: process.env.AI_TEMPERATURE ? parseFloat(process.env.AI_TEMPERATURE): 0.6,
    max_tokens: process.env.AI_MAX_TOKENS ? parseInt(process.env.AI_MAX_TOKENS): 256,
    top_p: process.env.AI_TOP_P ? parseFloat(process.env.AI_TOP_P): 1,
    frequency_penalty: process.env.AI_FRECUENCY_PENALTY ? parseFloat(process.env.AI_FRECUENCY_PENALTY): 0,
    presence_penalty: process.env.AI_PRESENCE_PENALTY ? parseFloat(process.env.AI_PRESENCE_PENALTY): 0
  },
  activePrompt: false
};

export const commentSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    // Action to add comment
    addComment: (state:any, action) => {
      state.value = [...state.value, action.payload]
    },
    resetComments: (state:any) => {
      state.value = []
    },
    initChat: (state:any, action) => {
      state.startChat = action.payload
    },
    updateActivePrompt: (state:any, action) => {
      state.activePrompt = action.payload
    },
    updatePayload: (state:any, action) => {
      state.payloadChatGPT = {...state.payloadChatGPT, ...action.payload}
    },
    resetPayload: (state:any) => {
      state.payloadChatGPT = initialState.payloadChatGPT
    },
    updatePayloadPrompt: (state:any, action) => {
      state.payloadPrompt = [...action.payload]
    },
  },
});

export const { addComment, resetComments, initChat, updatePayload, updatePayloadPrompt, resetPayload, updateActivePrompt } = commentSlice.actions
export const selectComments = (state:AppState) => state.comments.value
export const selectInitComments = (state:AppState) => state.comments.initial
export const selectStartChat = (state:AppState) => state.comments.startChat
export const selectPayloadChatGPT = (state:AppState) => state.comments.payloadChatGPT
export const selectPayloadPrompt = (state:AppState) => state.comments.payloadPrompt
export const selectActivePrompt = (state:AppState) => state.comments.activePrompt

export default commentSlice.reducer